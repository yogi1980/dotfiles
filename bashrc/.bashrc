#
# ~/.bashrc
#

### If not running interactively, don't do anything
[[ $- != *i* ]] && return

### History settings
# don't put duplicate lines or lines starting with space in the history
HISTCONTRO=ignoreboth
# setting unlimited history length
HISTSIZE=-1
HISTFILESIZE=-1

### SHOPT
# append to the history file, don't overwrite it
shopt -s histappend
# check the window size after each command
shopt -s checkwinsize

# Prompt
PS1='\e[01;33m\[ |\W| -> \e[m'


### Aliases
# changing "ls" to "exa"
alias ls='exa -alUm --header --color=always --group-directories-first --sort=extension'
# colorize grep output
alias grep='grep -n --color=auto'
# expressvpn shortcuts
alias vpnsm='expressvpn connect smart'
alias vpnpl='expressvpn connect pl'
alias vpndc='expressvpn disconnect'
alias vpnst='expressvpn status'
alias vpnls='expressvpn list all'
# check my global IP address
alias myip='curl https://ident.me/; echo'
# ranger
alias r='ranger'
# confirm before overwriting something
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
# gpg encryption
alias gpg-c="gpg -c --no-symkey-cache --cipher-algo TWOFISH"
# weather forecast
alias wttr="curl 'wttr.in/Inowroclaw?mF&lang=en'"
